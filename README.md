# C4-PlantUML

#### 介绍
* 这是PlantUML C4库的国内同步镜像，目前暂时1天1更，如果cron断了请提issue
* 原始仓库地址 https://github.com/plantuml-stdlib/C4-PlantUML
* 与其他镜像仓库不通，我修改了所有关联地址(到gitee)，以便可以在线流畅渲染
* 引用例子：
```
https://gitee.com/coder4/C4-PlantUML/raw/master/mirror/C4_Context.puml
https://gitee.com/coder4/C4-PlantUML/raw/master/mirror/C4_Container.puml
......
```
* 画图教程，[使用PlantUML(插件)绘制C4Model风格的软件架构图](https://www.coder4.com/archives/7539)