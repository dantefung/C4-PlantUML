#!/bin/bash

# rebase
git pull --rebase

# sync code & modify
rm -rf ./mirror
git clone https://github.com/plantuml-stdlib/C4-PlantUML.git ./mirror
rm -rf ./mirror/.git
rm -rf ./mirror/.github

find ./mirror -name "*puml" -exec sed -i "s#raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master#gitee.com/coder4/C4-PlantUML/raw/master/mirror##g" {} +

# push
git add . 
git commit -m "sync mirror $(date '+%Y-%m-%d')"
git push origin master

